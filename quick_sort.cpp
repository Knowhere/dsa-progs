#include <iostream>
#include <string>

using namespace std;

void swap(float *ptr1, float *ptr2)
{
	float temp = *ptr1;
	*ptr1 = *ptr2;
	*ptr2 = temp;
}

int partition (float arr[], int l, int r)
{
    int pivot = arr[r];
    int i = (l - 1);   
    for (int j = l; j < r; j++) {
		if (arr[j] < pivot) {
			i++;	
			swap(&arr[i], &arr[j]);
		}
	}
    swap(&arr[i + 1], &arr[r]);
    return (i + 1);
}

void quick_sort(float arr[], int p, int n) {
	if(p<n){
		int pivot;
		pivot = partition(arr, p, n);
		quick_sort(arr, p, pivot-1);
		quick_sort(arr, pivot+1, n);
	}
}

void display(float arr[], int size)
{
	cout<<"Top 5 Scores are:"<<endl;
	for (int i=size-1; i > size-6 && i >= 0; i--)
		cout<<arr[i]<<" \t";
	cout<<endl;
}

void accept(float arr[], int size)
{
	for (int i=0; i < size; i++)
		cin>>arr[i];
	cout<<endl;
}

int main () {
	int size;
	float arr[50];
		
	cout<<"\n\nUsing Quick sort:"<<endl<<"Enter Number of students: \t";
	cin>>size;
	cout<<endl<<"Enter percentage:"<<endl;
	accept(arr, size);
	quick_sort(arr, 0, size-1);
	cout<<"Sorted array: \n"<<endl;
	display(arr, size);
	
	return 0;
}
