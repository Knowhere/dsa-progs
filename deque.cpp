#include <iostream>
#include <stdlib.h>
#define MAX 5
using namespace std;

class Queue{
	int arr[MAX];
	int front;
	int rear;
  public:
  	Queue(){
  		front = rear = -1;
  	}
  	
  	void __init(){
  		front = rear = -1;
  	}
	
	void push_front(int data){
		if(!full())
			if(front > 0)
				front--;
			else{
				cout<<"Element not inserted. No available spaces at front"<<endl<<endl;
				return;
			}
			arr[front] = data;
			cout<<"Element inserted into Queue"<<endl<<endl;
	}
	int pop_front(){
		if(!empty()){
			int temp = arr[front++];
			if(empty() == true)
				__init();
			cout<<"Element ["<<temp<<"] Removed"<<endl<<endl;
			return temp;
		}
	}
	
	void push_rear(int data){
		if(!full()){
			if(front == -1)
				front++;
			arr[++rear] = data;
			cout<<"Element inserted into Queue"<<endl<<endl;
		}else{
			cout<<"Element not inserted. No available spaces at Rear"<<endl<<endl;
		}
	}
	
	int pop_rear(){
		if(!empty()){
			int temp = arr[rear--];
			if(empty() == true)
				__init();
			cout<<"Element ["<<temp<<"] Removed"<<endl<<endl;
			return temp;
		}
	}
	
	bool empty(){
		if(front > rear && front > -1)
			return true;
		else
			return false;
	}
	
	bool full(){
		if( rear >=  MAX-1 )
			return true;
		else
			return false; 
	}
	
	int size(){
		return (rear - front + 1);
	}
	
	int getfront(){
		return arr[front];
	}
	
	int getrear(){
		return arr[rear];
	}
	
	void display(){
		if(front == -1){
			cout<<"No Elements in the Queue";
		}else{
			cout<<endl<<"Queue: \t";
			for(int i = front; i <= rear; i++ ){
				cout<<arr[i]<<"\t";
			}
		}
	}
};

int main(){
	Queue que;
	int choice, ch, temp;
		cout<<"DeQue Implementation using Array"<<endl<<endl;
	while(1){
		cout<<"1. Insert into Queue\n2. Remove from the Queue\n3. Display Queue\n4. Flush Queue\n5. Exit"<<endl<<endl;
		cout<<"Enter your choice:\t";
		cin>>choice;
		switch(choice){
			case 1:
				cout<<"Enter Element to be inserted into the Queue:\t";
				cin>>temp;
				cout<<"1. Enter at Front \n2. Enter at End"<<endl<<endl;
				cin>>ch;
				switch(ch){
					case 1:
						que.push_front(temp);
						break;
					case 2:
						que.push_rear(temp);
						break;
					default :
						cout<<"Invalid Choice! Aborting the operation"<<endl<<endl;
						break;
				}
				break;
			case 2:
				cout<<"1. Remove from Front \n2. Remove from End"<<endl<<endl;
				cin>>ch;
				switch(ch){
					case 1:
						que.pop_front();
						break;
					case 2:
						que.pop_rear();
						break;
					default :
						cout<<"Invalid Choice! Aborting the operation"<<endl<<endl;
						break;
				}
				break;
			case 3:
				que.display();
				cout<<endl<<endl;
				break;
			case 4:
				que.__init();
				cout<<"Queue Flushed"<<endl<<endl;
				break;
			case 5:
				exit(0);
				break;
			default:
				cout<<"Enter Right Choice"<<endl<<endl;
				break;
		}
	}
	return 0;
}