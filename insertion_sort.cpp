#include <iostream>
#include <string>

using namespace std;

void swap(float *ptr1, float *ptr2)
{
	float temp = *ptr1;
	*ptr1 = *ptr2;
	*ptr2 = temp;
}


void print(float arr[], int size){
	for (int i = 0; i < size; i++)
	{
		cout<<arr[i]<<"\t";
	}
	cout<<endl;
}


void insertion_sort(float arr[], int n)
{
	int i, key, j;
	for (i = 1; i < n; i++) {
		key = arr[i];
		j = i-1;
 
		/* shifting */
		for (j = i-1 ; j >= 0 && arr[j] > key; j--)
			arr[j+1] = arr[j];
		arr[j+1] = key;

		cout<<"Iteration "<<i<<"\t";
		print(arr, n);
	}
}

void shell_sort(float arr[], int n) {
	int i, gap, j;
	for (gap = n/2; gap > 0; gap /= 2) {
		for (i = gap; i < n; i++) {
			float key = arr[i];

			/* shifting */
			for (j = i; j >= gap && arr[j - gap] > key; j = j - gap)
				arr[j] = arr[j - gap];
			arr[j] = key;
		}
		cout<<"Iteration \t";
		print(arr, n);
	}
}


void display(float arr[], int size)
{
	cout<<"Top 5 Scores are:"<<endl;
	for (int i=size-1; i > size-6 && i >= 0; i--)
		cout<<arr[i]<<" \t";
	cout<<endl;
}

void accept(float arr[], int size)
{
	for (int i=0; i < size; i++)
		cin>>arr[i];
	cout<<endl;
}

int main () {
	int size;
	float arr[50];
	
	
	cout<<"\n\nUsing Insertion sort:"<<endl<<"Enter Number of students: \t";
	cin>>size;
	cout<<endl<<"Enter percentage:"<<endl;
	accept(arr, size);
	insertion_sort(arr, size);
	cout<<"Sorted array: \n"<<endl;
	display(arr, size);
	
	cout<<"\n\nUsing Shell sort:"<<endl<<"Enter Number of students: \t";
	cin>>size;
	cout<<endl<<"Enter percentage:"<<endl;
	accept(arr, size);
	shell_sort(arr, size);
	cout<<"Sorted array: \n"<<endl;
	display(arr, size);
	
	return 0;
}
