#include <iostream>
#include <stdlib.h>
#define MAX 30
using namespace std;

class Queue{
	int arr[MAX];
	int front;
	int rear;
  public:
  	Queue(){
  		front = rear = -1;
  	}
  	
  	void __init(){
  		front = rear = -1;
  	}
	
	void push(int data){
		if(!full())
			if(front == -1)
				front++;
			arr[++rear] = data;
	}
	
	int pop(){
		if(!empty()){
			int temp = arr[front++];
			if(empty() == true)
				__init();
			cout<<"Job ID ["<<temp<<"] Removed"<<endl<<endl;
			return temp;
		}
	}
	
	bool empty(){
		if(front > rear)
			return true;
		else
			return false;
	}
	
	bool full(){
		if( rear >=  MAX )
			return true;
		else
			return false; 
	}
	
	int size(){
		return (rear - front + 1);
	}
	
	int getfront(){
		return arr[front];
	}
	
	int getrear(){
		return arr[rear];
	}
	
	void display(){
		if(front == -1){
			cout<<"No Job IDs in the Queue";
		}else{
			cout<<endl<<"Queue: \t";
			for(int i = front; i <= rear; i++ ){
				cout<<arr[i]<<"\t";
			}
		}
	}
};

int main(){
	Queue que;
	int choice, temp;
		cout<<"**********Job Queue**********"<<endl;
	while(1){
		cout<<"1. Insert into Queue\n2. Remove from the Queue\n3. Display Queue\n4. Flush Queue\n5. Exit"<<endl<<endl;
		cout<<"Enter your choice:\t";
		cin>>choice;
		switch(choice){
			case 1:
				cout<<"Enter Job ID to be inserted into the Queue:\t";
				cin>>temp;
				que.push(temp);
				cout<<"Job ID inserted into Queue"<<endl<<endl;
				break;
			case 2:
				que.pop();
				break;
			case 3:
				que.display();
				cout<<endl<<endl;
				break;
			case 4:
				que.__init();
				cout<<"Queue Flushed"<<endl<<endl;
				break;
			case 5:
				exit(0);
				break;
			default:
				cout<<"Enter Right Choice"<<endl<<endl;
				break;
		}
	}
	return 0;
}
