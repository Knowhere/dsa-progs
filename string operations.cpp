#include <iostream>
#include <cstring>
#include <cstdio>

using namespace std;

class Strings{
	char str[100],str2[100], str3[100];
	int i;
	public:

	void accept(){
		cout<<"Enter first String:\t";
		cin>>str;
		cout<<"Enter second String:\t";
		cin>>str2;
	}

	void display(){
		cout<<"String 1 is:\t"<<str<<endl;
		cout<<"String 2 is:\t"<<str2<<endl;
	}

	int length(char string[]){
		int length=0;
		while(string[length] != '\0')
			length++;
		return length;
	}

	void copy(){
		int len1 = length(str);
		for(i = 0; i<len1; i++)
			str2[i] = str[i];
		str2[i] = '\0';
		display();
		cout<<"String copied in Str2 as :"<<str2<<endl;
	}

	void concat(){
		int len1 = length(str);
		int len2 = length(str2);
		for(i = 0; i<len2; i++)
			str[len1+i] = str2[i];
		str[len1+i] = '\0';
	}

	void get_length()
	{
		cout<<"Length of the first string is: "<<length(str)<<endl;
		cout<<"Length of the second string is: "<<length(str2)<<endl;
	}
	

	void reverse()
	{
		int len1 = length(str);
		for(i = 0; i<len1; i++)
			str2[len1-(i+1)] = str[i];
		str2[i] = '\0';
		display();
		cout<<"String reversed in Str2 as :"<<str2<<endl;
	}
	
	void equal()
	{
		int flag = 0;
		int len1 = length(str);
		int len2 = length(str2);
		if(len1 == len2)
		{
			for(i = 0; i<len1; i++)
				if(str[i] != str2[i])
					goto not_equal;
			if(i==len1)
				cout<<"strings are equal"<<endl;
		}else{
			not_equal:
			cout<<"strings are not equal"<<endl;
		}
	}

	void substring(){
		int j=0;
		for(i=0 ; i<length(str); i++)
			if (str2[j]==str[i])
				j++;
			if(str2[j]!='\0')
				cout<<"substring not found";
			else
				cout<<"substring "<<str2<<" is found in "<<str<<endl;
		}
};

int main()
{
	int choice;
	Strings s1;
	s1.accept();
	do
	{
			cout<<"1. Display Lengths \n2. Copy 1st string in Str2 \n3. Concatinate first and second string \n4. Display strings 1 and 2 \n5. Reverse first string into str2 \n6. Check Equality \n7. Substring \n8. Accept New Strings \n9. Exit"<<endl<<endl;
			cout<<"Enter your Choice :\t";
			cin>>choice;
			cout<<endl;
			switch(choice)
			{
				case 1:
					s1.get_length();
					break;
				case 2:
					s1.copy();
					cout<<"Sting Copied"<<endl<<endl;
					break;
				case 3:
					s1.concat();
					cout<<"Sting Concatinated"<<endl<<endl;
					break;
				case 4:
					s1.display();
					break;
				case 5:
					s1.reverse();
					break;
				case 6:
					s1.equal();
					break;
				case 7:
					s1.substring();
					break;
				case 8:
					s1.accept();
				case 9:
					cout<<"Program Terminated!"<<endl<<endl;
					break;
				default:
					cout<<"Enter Valid Choice!"<<endl;
					break;
			}
			cout<<endl;
	}while(choice != 9);
	return 0;
}
