#include <iostream>
#include <cstdlib>
#include <cstring>

using namespace std;

struct node{
	int data; 
	struct node *next;
};

class linked_list{
public:
	node *head = NULL;
	void insert();
	void insert(node *);
	
	int deletenode();
	void display();
	node* create();
	int count_members();
	void sort();
	void swap(int *, int *);
	void divide(linked_list *, linked_list *);
	friend void merge(linked_list *, linked_list *, linked_list *);
};

void merge(linked_list *l1, linked_list *l2, linked_list *l3){
	node *trav, *tmp;
	l1->head = NULL;

	trav = l2->head;
	while(trav != NULL){
		tmp = trav;
		trav = trav->next;
		l1->insert(tmp);
	}
	trav = l3->head;
	while(trav != NULL){
		tmp = trav;
		trav = trav->next;
		l1->insert(tmp);
	}
	l2->head = NULL;
	l3->head = NULL;
}

node* linked_list::create()
{
	node *newnode = new node;
	cout<<"\nEnter number to be inserted: \t";
	cin>>newnode->data;
	newnode->next = NULL;
	return newnode;
}


void linked_list::insert()
{	
	node *newnode = create();
	if(head == NULL)
	{
		head = newnode;
	}else{
		node *trav = head;
		while(trav->next != NULL)
			trav = trav->next;
		trav->next = newnode;
	}
}

void linked_list::insert(node *newnode)
{	
	newnode->next = NULL;
	if(head == NULL)
	{
		head = newnode;
	}else{
		node *trav = head;
		while(trav->next != NULL)
			trav = trav->next;
		trav->next = newnode;
	}
}


int linked_list::deletenode()
{
	node *ptr = 0, *tmp, *trav = head;
	if(head == NULL){
		cout<<"No Elements!";
	}else{
		while(trav != NULL){
			if (trav->data < 0)
			{
				if (ptr != 0) {
					ptr->next = trav->next;
				} else {
					head = trav->next;
				} 
				tmp = trav; 
				trav = trav->next;
				delete tmp;
			} else {
				ptr = trav;
				trav = trav->next;
			}
		}
	}
	cout<<endl<<endl;
}

void linked_list::display(){
	node *trav = head;
	if(head == NULL){
		cout<<"No Elements!";
	}else{
		while(trav != NULL){
			cout<<"[ "<<trav->data<<" ] \t";
			if(trav->next != NULL)
			{
				cout<<"->\t";
			}
			trav = trav->next;
		}
	}
	cout<<endl<<endl;
}

void linked_list::swap(int *a, int *b){
    int temp;
    temp = *a;
    *a = *b;
    *b = temp;
}


void linked_list::sort(){
	node *ptr, *temp, *trav, *a, *b;
	ptr  = NULL;
	trav = head;
	while(trav!= NULL){
		a = trav;	
		b = a->next;
		while( b != NULL )
		{
			if( a->data > b-> data ){
				swap(&a->data, &b->data);
			}
			b = b->next;
		}
		trav = trav->next;
	}
}

int linked_list::count_members(){
	node *trav = head;
	int count = 0;
	if(head == NULL){
		cout<<"No Elements!"<<endl;
	}else{
		while(trav != NULL){
			count ++;
			trav = trav->next;
		}
	}
	return count;
}

void linked_list::divide(linked_list *l2, linked_list *l3)
{
	node *ptr = 0, *tmp, *trav = head;
	if(head == NULL){
		cout<<"No Elements!";
	}else{
		while(trav != NULL){
			if (trav->data < 0)
			{
				tmp = trav; 
				trav = trav->next;
				l2->insert(tmp);
			} else {
				tmp = trav;
				trav = trav->next;
				l3->insert(tmp);
			}
		}
	}
	cout<<endl<<endl;
}

int main(){
	linked_list l1, l2, l3;
	int choice, pos, ch;
	while(1){
		cout<<"1. Insert New node into Linked list \n2. Delete negative nodes From list \n3. Separate Lists \n4. Sort and merge \n5. Display List\n6. Accept new List \n7. Exit"<<endl<<endl;
		cout<<"Enter your choice:\t";
		cin>>choice;	
		switch(choice){
			case 1:	
				l1.insert();
				cout<<"New Member Inserted."<<endl;
				break;
			case 2:
				l1.deletenode();
				cout<<endl<<"Deleted all Negative nodes from List 1"<<endl<<endl;

				break;
			case 3:
				l1.divide(&l2, &l3);
				cout<<endl<<"List 1 is Separated into List 2 and List 3"<<endl<<endl;
				break;

			case 4:
				//l2.sort();
				//l3.sort();
				merge(&l1, &l2, &l3);
				cout<<endl<<"List 2 and List 3 are merged into List 1"<<endl<<endl;
				break;
			case 5:
				cout<<endl<<"List 1 :\t ";
				l1.display();
				cout<<endl<<"List 2 :\t ";
				l2.display();
				cout<<endl<<"List 3 :\t ";
				l3.display();
				break;
			case 6:
				l1.head == NULL;
				l1.insert();
				break;
			case 7:
				cout<<"bye"<<endl<<endl;
				exit(0);
				break;
			default:
				cout<<"Enter Right Choice"<<endl<<endl;
				break;
		}
	}
	return 0;
}


