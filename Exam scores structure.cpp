#include<iostream>

using namespace std;

struct student{
	int roll,marks;
	char attendance;
};

int main(){
	int choice, n, sum, i;
	int highest, lowest;
	int result[20], max, temp, j, count; 
	student s[30];
	cout<<"Enter No. of students to be added:\t";
	cin>>n;
	for(i=0;i<n;i++)
	{
		cout<<"Enter Roll No.\t";
		cin>>s[i].roll;
		cout<<"Was Roll No. "<<s[i].roll<<" Present for the exam? (p or a)\t";
		cin>>s[i].attendance;
		if(s[i].attendance == 'p' || s[i].attendance == 'p')
		{
			cout<<"Enter marks\t";
			cin>>s[i].marks;
		}
	}
	do{ 
		cout<<"\n\n1. Average Score \n2. Highest and lowest Score\n3. Marks scored by most students \n4. List of Absent Students \n5. Accept new list \n6. Display Current List\n7. Exit";
		cout<<"\n\nEnter your choice\t\t";
		cin>>choice;
		switch(choice)
		{
			case 1: 
				sum = 0;
				count = 0;
				for(i=0; i<n; i++)
				{
					if(s[i].attendance == 'p' || s[i].attendance == 'P'){
						count++;
						sum = sum + s[i].marks;
					}
				}
				sum = sum/count;
				cout<<"\n\nAverage Score is: "<<sum;
				break;
			
			
			case 2: 
				highest=s[0].marks;
				lowest=s[0].marks;
				for(i=1; i<n; i++)
				{
					if(s[i].attendance == 'p' || s[i].attendance == 'P')
						if(s[i].marks > highest)
							highest = s[i].marks;
				}
				for(i=1; i<n; i++)
				{
					if(s[i].attendance == 'p' || s[i].attendance == 'P')
						if(s[i].marks < lowest)
						lowest = s[i].marks;
				}
				cout<<"Highest Score is "<<highest<<endl;
				cout<<"\tLowest Score is "<<lowest<<endl;
				break;
			

			case 3: 
				for(i=0;i<n;i++)
				{
					count= 0;
					for(j=0;j<n;j++)
					{
						if(s[i].attendance == 'p' || s[i].attendance == 'P')
							if(s[i].marks == s[j].marks)
							{
								count++;
								result[i]=count;
								temp=s[i].marks;
							}					
					}
				}
				max=result[0];
				for(i=0;i<n;i++)
					if(max<result[i])
					{
						max=result[i];
					}
				cout<<"max marks ("<<temp<<") scored by most students: \t"<<max<<endl;
				break;
			

			case 4:
				cout<<"Current List:\n\nRoll No.\n\n";
				for(i=0;i<n;i++)
				{
					if(s[i].attendance == 'a' || s[i].attendance == 'A')
					{
						cout<<s[i].roll<<endl;
					}
				} 
				break;
			

			case 5: 
				for(i=0;i<n;i++)
				{
					cout<<"Enter Roll No.\t";
					cin>>s[i].roll;
					cout<<"Was Roll No. "<<s[i].roll<<" Present for the exam? (p or a)\t";
					cin>>s[i].attendance;
					if(s[i].attendance == 'p' || s[i].attendance == 'p')
					{
						cout<<"Enter marks\t";
						cin>>s[i].marks;
					}else{
						s[i].marks = 0;
					}
				}
				break;
			

			case 6: 
				cout<<"Current List:\n\nRoll No.\t\tMarks\t\t Attendance\n\n";
				for(i=0;i<n;i++)
				{
					cout<<s[i].roll<<"\t\t\t"<<s[i].marks<<"\t\t";
					if(s[i].attendance == 'p' || s[i].attendance == 'P')
					{
						cout<<"Present\n";
						
					}else{ 
						cout<<"Absent\n";
					}
				}
				break;
			

			case 7: 
				cout<<"Bye!\n\n\n";
				break;
			

			default: 
				cout<<"Wrong Chioce!\n\n";
				break;				
		}
	}while(choice!=7);
	return 0;
}
