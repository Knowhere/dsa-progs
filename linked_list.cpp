#include <iostream>
#include <cstdlib>

using namespace std;

struct node{
	int data;
	struct node *next;
};

class linked_list{
public:
	node *head = NULL;
	void insertbeg();
	void insertmiddle(int);
	void insertend();
	
	int deletebeg();
	int deletemiddle(int);
	int deleteend();
	
	int locate(int key);
	void display();
	node* create();
};

node* linked_list::create()
{
	node *newnode = new node();
	cout<<"\nEnter the number to be inserted\n";
	cin>>newnode->data;
	newnode->next = NULL;
}

void linked_list::insertbeg()
{
	node *newnode = create();
	if(head == NULL)
	{
		newnode->next = NULL;
	}else{
		newnode->next = head;
	}
	head = newnode;
}

void linked_list::insertmiddle(int pos){
	node *trav = head;
	int i = 1;
	while(i != pos-1)
	{
		i++;
		trav = trav->next;
	}
	node *newnode =  create();
	newnode->next = trav->next;
	trav->next = newnode;
}

void linked_list::insertend(){
	node *newnode =  create();
	if(head == NULL)
	{
		head = newnode;
	}else{
		node *trav = head;
		while(trav->next != NULL)
			trav = trav->next;
		trav->next = newnode;
	}
}

int linked_list::deletebeg(){
	node *temp = head;
	if(head != NULL){
		head = head->next;
		free(temp);
	}else{
		cout<<"No elements!"<<endl<<endl;
	}
}

int linked_list::deleteend(){
	node *ptr, *trav = head;
	if(head->next != NULL){
		while(trav->next != NULL){
			ptr = trav;
			trav = trav->next;
		}
			ptr->next = NULL;
			free(trav);
		}else{
			head = NULL;
		}
}

int linked_list::deletemiddle(int pos)
{
	node *ptr, *trav = head;
	for(int i = 0;i<pos-1;i++)
	{
		ptr = trav;
		trav = trav->next;
	}
	ptr->next = trav->next;
	free(trav);
}

void linked_list::display(){
	node *trav = head;
	if(head == NULL){
		cout<<"No Elements!";
	}else{
		while(trav != NULL){
			cout<<trav->data<<"\t";
			trav = trav->next;
		}
	}
	cout<<endl<<endl;
}


int main(){
	linked_list l1;
	int choice, pos, ch;
	do{
		cout<<"1. Insert into Linked list \n2. Delete From list \n3. Locate in the list \n4. Display List\n5. Accept new List \n6. Exit"<<endl;
		cout<<"Enter your choice:\t";
		cin>>choice;	
		switch(choice){
			case 1:
				cout<<"1: Begining, 2: Middle, 3: End, 4: Abort"<<endl;
				cout<<"Enter your choice: \t";
				cin>>ch;
				switch(ch){
					case 1:				
						l1.insertbeg();
						cout<<"Inserted at Begining."<<endl;
						break;
					case 2:
						cout<<"Enter Position to be inserted at :";
						cin>>pos;
						l1.insertmiddle(pos);						
						cout<<"Inserted at Middle."<<endl;
						break;
					case 3:
						l1.insertend();
						cout<<"Inserted at End."<<endl;
						break;
				}				
				break;
			case 2:
				cout<<"1: Begining, 2: Middle, 3: End, 4: Abort";
				cout<<"Enter your choice: \t";
				cin>>ch;
				switch(ch){
					case 1:				
						l1.deletebeg();
						cout<<"deleteed at Begining."<<endl;
						break;
					case 2:
						cout<<"Enter Position to be deleteed at :";
						cin>>pos;
						l1.deletemiddle(pos);						
						cout<<"deleteed at Middle."<<endl;
						break;
					case 3:
						l1.deleteend();
						cout<<"deleteed at Begining."<<endl;
						break;
				}				
				break;
			case 3:
				break;
			case 4:
				l1.display();
				break;
			case 5:
				l1.head == NULL;
				l1.insertbeg();
				break;
			case 6:
				cout<<"bye"<<endl<<endl;
				break;
			default:
				cout<<"Enter Right Choice"<<endl<<endl;
				break;
		}
	}while(choice != 6);
	return 0;
}
