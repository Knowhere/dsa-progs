#include<iostream>
using namespace std;

struct student{
	char name[40];
	char cricket,badminton;
};

int main()
{
	student s[20];
	int n, choice;
	char ch;
	cout<<"Enter No of students to be accepted:\t";
	cin>>n;
	for(int i=0; i<n;i++)
	{
		cout<<"Enter Name of the Student\t\t";
		cin>>s[i].name;
		cout<<"Does "<<s[i].name<<" play Cricket? (y/n)\t";
		cin>>ch;
		s[i].cricket = ch;
		cout<<"Does "<<s[i].name<<" play Badminton? (y/n)\t";
		cin>>ch;
		s[i].badminton = ch;
	}

	do{
		cout<<"\n\n1. (A) U (B) \n2. (A) ^ (B) \n3. (A) \n4. (B) \n5. ((A) U (B))` \n6. Display list \n7. Accept New Student List \n8. Exit\n\n";
		cout<<"Enter Your choice: \t";
		cin>>choice;
		switch(choice){
			case 1:
				cout<<"List of Students who play Cricket or Badminton or Both:\n";
				for(int i=0;i<n;i++)
				{
					if(s[i].cricket == 'y' || s[i].badminton == 'y')
						cout<<s[i].name<<"\n";
				}			
				break;
			case 2:
				cout<<"List of Students who play Cricket AND Badminton:\n";
				for(int i=0;i<n;i++)
				{
					if(s[i].cricket == 'y' && s[i].badminton == 'y')
						cout<<s[i].name<<"\n";
				}			
				break;
			case 3:
				cout<<"List of Students who play Cricket:\n";
				for(int i=0;i<n;i++)
				{
					if(s[i].cricket == 'y')
						cout<<s[i].name<<"\n";
				}
				break;
			case 4:
				cout<<"List of Students who play Badminton:\n";
				for(int i=0;i<n;i++)
				{
					if(s[i].badminton == 'y')
						cout<<s[i].name<<"\n";
				}			

				break;
			case 5:
				cout<<"List of Students who play neither Cricket nor Badminton:\n";
				for(int i=0;i<n;i++)
				{
					if(s[i].cricket == 'n' && s[i].badminton == 'n')
						cout<<s[i].name<<"\n";
				}			

				break;
			case 6:
					cout<<"\nAcccepted List:\n";
					cout<<"\n\nName\t\tCricket\t\tBadminton\n";
					for(int i=0;i<n;i++)
					{
						cout<<s[i].name<<"\t\t"<<s[i].cricket<<"\t\t"<<s[i].badminton<<"\n";
					}
				break;
			case 7:
				cout<<"Enter No of students to be accepted:\t";
				cin>>n;
				for(int i=0; i<n;i++)
				{
					cout<<"Enter Name of the Student\t\t";
					cin>>s[i].name;
					cout<<"Does "<<s[i].name<<" play Cricket? (y/n)\t";
					cin>>ch;
					s[i].cricket = ch;
					cout<<"Does "<<s[i].name<<" play Badminton? (y/n)\t";
					cin>>ch;
					s[i].badminton = ch;
				}				
				break;
			case 8:
				cout<<"Bye!\n\n";
				break;
			default:
				cout<<"Enter a right choice!\n";
				break;
		};	
	}while(choice != 8);
}
