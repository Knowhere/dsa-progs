//C program to Convert given INFIX expression into PREFIX expression using stack// 

#include<iostream>
#include<cstring>
#include<cstdio>
#include<stdlib.h>
using namespace std;

#define MAX 40
struct stack_struct{
	char array[MAX];
	int top;	
}stack;

char pop();
void push(char item);

/*
char *strrev(char *str){
	char c, *front, *back;

	if(!str || !*str)
		return str;
	for(front=str,back=str+strlen(str)-1;front < back;front++,back--){
		c=*front;
		*front=*back;
		*back=c;
	}
	return str;
}
*/
int isoperator(char symbol) {
	switch(symbol) {
		case '(':
		case ')':
		case '[':
		case ']':
		case '{':
		case '}':
			return 1;
		default:
			return 0;
}
}

void check_balance(char infix[]) {
	int i,symbol;
	push('#');
	for(i=0;i<strlen(infix);i++) {
		symbol=infix[i];
		if(isoperator(symbol)!=0) {
			if(symbol=='(') {
				push(symbol);
			}else if(symbol==')') {
				while(stack.array[stack.top]!='(' ) {
					pop();
				}
				pop();//pop out (.
			}else if(symbol=='[') {
				push(symbol);
			}else if(symbol==']') {
				while(stack.array[stack.top]!='[' ) {
					pop();
				}
				pop();//pop out [.
			}else if(symbol=='{') {
				push(symbol);
			}else if(symbol=='}') {
				while(stack.array[stack.top]!='{' ) {
					pop();
				}
				pop();//pop out (.
			}else{

			}//end of else.
		}//end of else.
	}//end of for.
}//end of function definition

int main() {
	char infix[20];
	stack.top = -1;
	cout<<"Enter the valid infix string:\n"<<endl;
	cin>>infix;
	check_balance(infix);
	if( stack.array[stack.top] == '#' )
		cout<<"Well Paranthesized"<<endl;
	else
		cout<<"Poorly Paranthesized expression"<<endl;
	return 0;
}

void push(char item) {
	if(stack.top != 39){
		stack.top++;
		stack.array[stack.top]=item;
	}else{
		cout<<"stack Overflowed!"<<endl;
		cout<<"Poorly Paranthesized expression"<<endl<<endl;
		exit(0);
	}
}

char pop() {
	if(stack.top != 0){
		char a;
		a=stack.array[stack.top];
		stack.top--;
		return a;
	}else{
		cout<<"stack Underflowed!"<<endl;
		cout<<"Poorly  Paranthesized expression"<<endl<<endl;
		exit(0);
	}
}
