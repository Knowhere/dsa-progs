//C program to Convert given INFIX expression into POSTFIX expression using stack// 

#include<iostream>
#include<cstring>
#include<cstdio>
#include<stdlib.h>
using namespace std;

#define MAX 40
template <class T>
struct stack_struct{
	T array[MAX];
	int top;	

	void push(T item) {
		if(top != 39){
			top++;
			array[top]=item;
		}else{
			cout<<"stack Overflowed!"<<endl;
			cout<<"Poorly Paranthesized expression"<<endl<<endl;
			exit(0);
		}
	}

	T pop() {
		if(top != 0){
			T a;
			a=array[top];
			top--;
			return a;
		}else{
			cout<<"stack Underflowed!"<<endl;
			cout<<"Poorly Paranthesized expression"<<endl<<endl;
			exit(0);
		}
	}

	void flush(){
		top == -1;
	}

};
stack_struct<char> stack;
stack_struct<int> int_stack;
/*char *strrev(char *str){
	char c, *front, *back;

	if(!str || !*str)
		return str;
	for(front=str,back=str+strlen(str)-1;front < back;front++,back--){
		c=*front;
		*front=*back;
		*back=c;
	}
	return str;
}*/

int prcd(char symbol) {
	switch(symbol) {
		case '+':
		case '-':
			return 2;
		case '*':
		case '/':
			return 4;
		case '^':
			return 6;
		case '(':
		case ')':
		case '#':
			return 1;
	}
}


int isoperator(char symbol) {
	switch(symbol) {
		case '+':
		case '-':
		case '*':
		case '/':
		case '^':
		case '(':
		case ')':
			return 1;
		default:
			return 0;
	}
}

void conv_into_postf(char infix[],char postfix[]) {
	int i,symbol,j=0;
	stack.	push('#');

	for(i=0;i<strlen(infix);i++) {
		symbol=infix[i];
		if(isoperator(symbol)==0) {
			postfix[j]=symbol;
			j++;
		}else {
			if(symbol=='(') {
				stack.push(symbol);
			}else if(symbol==')') {
				while(stack.array[stack.top]!='(') {
					postfix[j]=stack.pop();
					j++;
				}
				stack.pop();//pop out (.
			}else if(isoperator(symbol) == 1){
				if(prcd(symbol)>prcd(stack.array[stack.top])) {
					stack.push(symbol);
				}else {
					while(prcd(symbol)<=prcd(stack.array[stack.top])) {
						postfix[j]=stack.pop();
						j++;
					}
					stack.push(symbol);
				}
			}else{
				cout<<"Character Exception!"<<symbol<<endl<<endl;
			}
		}
	}

	while(stack.array[stack.top]!='#') {
		postfix[j]=stack.pop();
		j++;
	}
	postfix[j]='\0';
}

int calculate(int a, int b, char symbol){
	switch(symbol){
		case '+':
			return a + b;
			break;
		case '-':
			return a - b;
			break;
		case '*':
			return a * b;
			break;
		case '/':
			return a / b;
			break;
		case '^':
			return a ^ b;
			break;
		default:
			return 0; 
	}
}

int evalualte(char expression[]){
	int_stack.flush();
	char symbol, *ptr, digit[3];
	ptr = &symbol;
	int operand1, operand2, result, j = 0;
	for(int i = 0; i<strlen(expression); i++) {
		symbol = expression[i]; // Read Token
			
		if(symbol >= '0' && symbol <= '9'){
			digit[j++] = symbol;
		}else if(symbol == ' '){
			if(j>0){
				digit[j] = '\0';
				int_stack.push(atoi(digit));
				j = 0;
			}
		}else {
			operand1 = int_stack.pop();
			operand2 = int_stack.pop();
			result = calculate(operand2, operand1, symbol);
			int_stack.push(result);
		}
	}
	return int_stack.pop();
}

int main() {
	char infix[20],postfix[20];
	stack.top = -1;
	cout<<"Enter the valid infix string:"<<endl<<endl;
	gets(infix);
	conv_into_postf(infix,postfix);
	cout<<"The corresponding postfix string is:"<<endl<<endl;
	puts(postfix);
	cout<<endl<<endl;
	cout<<"Evaluation:"<<endl<<endl;
	cout<<"Result:"<<evalualte(postfix)<<endl;
	return 0;
}
