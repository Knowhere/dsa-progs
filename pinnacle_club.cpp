#include <iostream>
#include <cstdlib>
#include <cstring>

using namespace std;

struct node{
	char name[40];
	int PRN;
	char year[30];
	char mem_type[50]; 
	struct node *next;
};

class linked_list{
public:
	node *head = NULL;
	void insert_president(node *);
	void insert_member(node *);
	void insert_secretary(node *);
	
	int delete_member(int);
	
	int locate(int key);
	void display();
	node* create();
	void count_members();
	void print_reverse(node *);
};

void linked_list::print_reverse(node *temp){
	if(temp == NULL)
		return;
	print_reverse(temp->next);

	cout<<"[ "<<temp->PRN<<" | "<<temp->name<<" ] \t";
	if(temp != head)
	{
		cout<<"->\t";
	}
}
 
void merge(linked_list *l1, linked_list *l2, linked_list *l3){
	node *trav, *tmp;
	l1->head = NULL;

	trav = l2->head;
	while(trav != NULL){
		tmp = trav;
		trav = trav->next;
		l1->insert_secretary(tmp);
	}
	trav = l3->head;
	while(trav != NULL){
		tmp = trav;
		trav = trav->next;
		l1->insert_secretary(tmp);
	}
	l2->head = NULL;
	l3->head = NULL;
}


node* linked_list::create()
{
	int choice;
	node *newnode = new node();
	cout<<"\nEnter Name of the Member: \t";
	cin>>newnode->name;
	cout<<"\nEnter PRN of the Member: \t";
	cin>>newnode->PRN;
	cout<<"\nEnter year of the Member: \t";
	cout<<"1. SE 2. TE 3. BE"<<endl;
	cin>>choice;
	switch(choice)
	{
		case 1:
			strcpy(newnode->year, "SE");
			break;
		case 2:
			strcpy(newnode->year, "TE");
			break;
		case 3:
			strcpy(newnode->year, "BE");
			break;
		default:
			"Enter right choice";
	}
	cout<<"\nEnter Membership Type: \t";
	newnode->next = NULL;
	cout<<"1. President 2. Memeber 3. Secratary"<<endl;
	cin>>choice;
	switch(choice)
	{
		case 1:
			strcpy(newnode->mem_type, "pres");
			insert_president(newnode);
			break;
		case 2:
			strcpy(newnode->mem_type, "mem");
			insert_member(newnode);
			break;
		case 3:
			strcpy(newnode->mem_type, "sec");
			insert_secretary(newnode);
			break;
		default:
			"Enter right choice";
	}
}

void linked_list::insert_president(node *newnode)
{

	if(head == NULL)
	{
		newnode->next = NULL;
	}else{
		newnode->next = head;
	}
	head = newnode;
}

void linked_list::insert_member(node *newnode){

	if(head == NULL)
	{
		head = newnode;
	}else{
		node *trav = head;
		while(trav->next != NULL && trav->mem_type == "pres"){
			cout<<trav->next->mem_type;
			trav = trav->next;
		}
		newnode->next = trav->next;
		trav->next = newnode;
	}
}

void linked_list::insert_secretary(node *newnode)
{
	if(head == NULL)
	{
		head = newnode;
	}else{
		node *trav = head;
		while(trav->next != NULL)
			trav = trav->next;
		trav->next = newnode;
	}
}


int linked_list::delete_member(int pos)
{
node *ptr, *trav = head;
	for(int i = 0;i<pos-1;i++)
	{
		ptr = trav;
		trav = trav->next;
	}
	ptr->next = trav->next;
	free(trav);
}

void linked_list::display(){
	node *trav = head;
	if(head == NULL){
		cout<<"No Elements!";
	}else{
		while(trav != NULL){
			cout<<"[ "<<trav->PRN<<" | "<<trav->name<<" ] \t";
			if(trav->next != NULL)
			{
				cout<<"->\t";
			}
			trav = trav->next;
		}
	}
	cout<<endl<<endl;
}

void linked_list::count_members(){
	node *trav = head;
	int count = 0;
	if(head == NULL){
		
	}else{
		while(trav != NULL){
			count ++;
			trav = trav->next;
		}
	}
	cout<<"in the Pinnacle Club: "<<count<<endl<<endl;
}


int main(){
	linked_list l1, l2, l3;
	int choice, pos, ch;
	while(1){
		cout<<"1. Insert New Member into Linked list \n2. Delete From list \n3. Count Members \n4. Merge both Lists \n5. Display Lists \n6. Display lists in reverse \n7. Accept new Lists \n8. Exit"<<endl;
		cout<<"Enter your choice:\t";
		cin>>choice;	
		switch(choice){
			case 1:
				cout<<"Do you want to Add member to Division A or B? [a-1 | b-2 ]\t";
				cin>>ch;
				switch(ch){
					case 1:
						l2.create();
						cout<<"New Member Inserted."<<endl<<endl;
						break;
					case 2:
						l3.create();
						cout<<"New Member Inserted."<<endl<<endl;
						break;
					default:
						cout<<"Operation aborted"<<endl<<endl;
				}
				break;
			case 2:
				cout<<"Do you want to Remove member from Division A or B? [a-1 | b-2 ]\t";
				cin>>ch;
				switch(ch){
					case 1:
						cout<<"Enter Position to be deleted at :";
						cin>>pos;
						l2.delete_member(pos);						
						break;
					case 2:
						cout<<"Enter Position to be deleted at :";
						cin>>pos;
						l2.delete_member(pos);						
						break;
					default:
						cout<<"Operation aborted"<<endl<<endl;
				}
				break;
			case 3:
				cout<<"No. of Memebers of Division A ";
				l2.count_members();
				cout<<"No. of Memebers of Division B ";
				l3.count_members();
				break;

			case 4:
				merge(&l1, &l2, &l3);
				cout<<"Division A and B merged into List 1";
				l1.display();
				break;
			case 5:
				cout<<"List of Memebers of Division A : \t";
				l2.display();
				cout<<"List of Memebers of Division B : \t";
				l3.display();
				break;
			case 6:
				cout<<"Reversal Display of Members: \n Div A: \t";
				l2.print_reverse(l2.head);
				cout<<endl<<"Div B: \t";
				l3.print_reverse(l2.head);
				cout<<endl<<endl;
				break;
			case 7:
				l2.head == NULL;
				l3.head == NULL;
				cout<<"Both lists truncated."<<endl;
				cout<<"Do you want to Add member to Division A or B? [a-1 | b-2 ]\t";
				cin>>ch;
				switch(ch){
					case 1:
						l2.create();
						cout<<"New Member Inserted."<<endl<<endl;
						break;
					case 2:
						l3.create();
						cout<<"New Member Inserted."<<endl<<endl;
						break;
					default:
						cout<<"Operation aborted"<<endl<<endl;
				}
				break;
			case 8:
				cout<<"bye"<<endl<<endl;
				exit(0);
				break;
			default:
				cout<<"Enter Right Choice"<<endl<<endl;
				break;
		}
	}
	return 0;
}

