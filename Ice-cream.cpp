#include<iostream>
#include<stdlib.h>
using namespace std;

struct node{
	int roll, vanilla, butterscotch;
	struct node *next;
};

class linked_list{
	public:
	void insert();
	node* create();
	node *head = NULL;
	void display_all();
	void display_union();
	void display_instersec();
	void display_a_min_b();
	void display_b_min_a();
	void display_none();
};

void linked_list::display_all(){
	node *trav = head;
	if(head == NULL){
		cout<<"No Elements!";
	}else{
		while(trav != NULL){
			cout<<trav->roll<<"\t";
			trav = trav->next;
		}
	}
	cout<<endl<<endl;
}

void linked_list::display_union(){
	node *trav = head;
	if(head == NULL){
		cout<<"No Elements!";
	}else{
		while(trav != NULL){
			if(trav->vanilla == 1 || trav->butterscotch == 1 ){
				cout<<trav->roll<<"\t";
			}
			trav = trav->next;
		}
	}
	cout<<endl<<endl;
}

void linked_list::display_instersec(){
	node *trav = head;
	if(head == NULL){
		cout<<"No Elements!";
	}else{
		while(trav != NULL){
			if(trav->vanilla == 1 && trav->butterscotch == 1 ){
				cout<<trav->roll<<"\t";
			}
			trav = trav->next;
		}
	}
	cout<<endl<<endl;
}

void linked_list::display_a_min_b(){
	node *trav = head;
	if(head == NULL){
		cout<<"No Elements!";
	}else{
		while(trav != NULL){
			if(trav->vanilla == 1 && trav->butterscotch == 0 ){
				cout<<trav->roll<<"\t";
			}
			trav = trav->next;
		}
	}
	cout<<endl<<endl;
}
void linked_list:: display_b_min_a(){
	node *trav = head;
	if(head == NULL){
		cout<<"No Elements!";
	}else{
		while(trav != NULL){
			if(trav->vanilla == 0 && trav->butterscotch == 1 ){
				cout<<trav->roll<<"\t";
			}
			trav = trav->next;
		}
	}
	cout<<endl<<endl;
}

void linked_list:: display_none(){
	node *trav = head;
	if(head == NULL){
		cout<<"No Elements!";
	}else{
		while(trav != NULL){
			if(trav->vanilla == 0 && trav->butterscotch == 0 ){
				cout<<trav->roll<<"\t";
			}
			trav = trav->next;
		}
	}
	cout<<endl<<endl;
}

node* linked_list::create()
{
	char choice = 'n';
	node *newnode = new node();
	cout<<"\nEnter the Roll No: \t";
	cin>>newnode->roll;
	cout<<"Do you like Vanilla Ice-cream? (y/n)\t";
	cin>>choice;
	if (choice == 'y' || choice == 'Y')
	{
		newnode->vanilla = 1;
	} else {
		newnode->vanilla = 0;
	}
	cout<<"Do you like Butterscotch Ice-cream? (y/n)\t";
	cin>>choice;
	if (choice == 'y' || choice == 'Y')
	{
		newnode->butterscotch = 1;
	} else {
		newnode->butterscotch = 0;
	}
	newnode->next = NULL;
}

void linked_list::insert()
{
	node *newnode =  create();
	if(head == NULL)
	{
		head = newnode;
	}else{
		node *trav = head;
		while(trav->next != NULL)
			trav = trav->next;
		trav->next = newnode;
	}
}

int main(){
	linked_list l1;
	int choice, pos;
	char ch;
	l1.insert();
	while(1){
		cout<<"1. Insert into Linked list \n2. Display Union \n3. Display Instersection \n4. Only Vannila \n5. Only Butterscotch \n6. None \n7. Display List \n8. Accept new List \n9. Exit"<<endl<<endl;
		cout<<"Enter your choice: \t";
		cin>>choice;	
		switch(choice){
			case 1:
				l1.insert();
				cout<<"Node Inserted"<<endl;
				break;
			case 2:
				//Union
				l1.display_union();
				break;
			case 3:
				//Intersection
				l1.display_instersec();
				break;
			case 4:
				// A-B
				l1.display_a_min_b();
				break;
			case 5:
				//B-A
				l1. display_b_min_a();
				break;
			case 6:
				//(A U B)`
				l1.display_none();
				break;
			case 7:
				//Display All
				l1.display_all();
				break;
			case 8:
				//accept new list
				l1.head == NULL;
				l1.insert();
				break;
			case 9:
				cout<<"bye"<<endl<<endl;
				exit(0);
			default:
				cout<<"Enter Right Choice!"<<endl<<endl;
				break;
		}
	}
	return 0;
}
