#include <iostream>
#include <string>

using namespace std;

void swap(float *ptr1, float *ptr2)
{
	float temp = *ptr1;
	*ptr1 = *ptr2;
	*ptr2 = temp;
}


void print(float arr[], int size){
	for (int i = 0; i < size; i++)
	{
		cout<<arr[i]<<"\t";
	}
	cout<<endl;
}


void select_sort(float arr[], int n)
{ 
	for (int i = 0; i < n-1; i++) {
		int min = i;
		for (int j = i+1; j < n; j++)
			if (arr[j] < arr[min])
				min = j;
 		swap(&arr[min], &arr[i]);

 		cout<<"Iteration "<<i<<"\t";
 		print(arr, n);
	}
}

void bubble_sort(float arr[], int n)
{ 
	for (int i = 0; i < n-1; i++) {
		for (int j = 0; j < n-i-1; j++)
			if (arr[j] > arr[j+1])
				swap(&arr[j], &arr[j+1]);

		cout<<"Iteration "<<i<<"\t";
 		print(arr, n);
	}
}

void display(float arr[], int size)
{
	cout<<"Top 5 Scores are:"<<endl;
	for (int i=size-1; i > size-6 && i >= 0; i--)
		cout<<arr[i]<<" \t";
	cout<<endl;
}

void accept(float arr[], int size)
{
	for (int i=0; i < size; i++)
		cin>>arr[i];
	cout<<endl;
}

int main () {
	int size;
	float arr[50];
	
	
	cout<<"\n\nUsing Selection sort:"<<endl<<"Enter Number of students: \t";
	cin>>size;
	cout<<endl<<"Enter percentage:"<<endl;
	accept(arr, size);
	select_sort(arr, size);
	cout<<"Sorted array: \n"<<endl;
	display(arr, size);
	
	cout<<"\n\nUsing Bubble sort:"<<endl<<"Enter Number of students: \t";
	cin>>size;
	cout<<endl<<"Enter percentage:"<<endl;
	accept(arr, size);
	bubble_sort(arr, size);
	cout<<"Sorted array: \n"<<endl;
	display(arr, size);
	
	return 0;
}
